function saveCanvasToPhotoLibrary()
{
    
    $("#saving").fadeIn();

    var canvas2ImagePlugin = window.plugins.canvas2ImagePlugin;
    canvas2ImagePlugin.saveImageDataToLibrary(
        function(msg) {
            $("#saving").fadeOut();
            // navigator.notification.alert("Saved to Photo Library 1200x1200px");
        },
        function(err) {
            navigator.notification.alert("Capture failed, I am filled with shame.");
        },
        'c'
    );
}

