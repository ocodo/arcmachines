// graphicsContext 

// function drawArc(g, _arcAngle, _startAngle, _radius, _thickness, drawX, drawY, width, height) {
function drawArc(g, settings, drawX, drawY, width, height) {

    var _arcAngle = settings.arcAngleA, _startAngle = settings.startAngleA, _radius = settings.radius, _thickness = settings.thickness;

	var _svg="";

	var xo=drawX; // + (width / 2);
	var yo=drawY; // + (width / 2);

	var workingArc=_arcAngle;
	// limit sweep to reasonable numbers
	if (Math.abs(workingArc) > 360)
	    workingArc=360;

	// Flash uses 8 segments per circle, to match that, we draw in a maximum
	// of 45 degree segments. First we calculate how many segments are needed
	// for our arc.
	var segs=Math.ceil(Math.abs(workingArc) / 45);

	// Now calculate the sweep of each segment.
	var segAngle_a=workingArc / segs
	var segAngle_b=-workingArc / segs;

	// The math requires radians rather than degrees. To convert from degrees
	// use the formula (degrees/180)*Math.PI to get radians.
	var theta_a=-(segAngle_a / 180) * Math.PI;
	var theta_b=-(segAngle_b / 180) * Math.PI;

	// convert angle workingAngle to radians
	var workingAngle=_startAngle;
	var angle=-(workingAngle / 180) * Math.PI;

	// draw the curve in segments no larger than 45 degrees.
	if (segs > 0)
	{
	    // draw a line from the end of the interior curve to the start of the exterior curve
	    var workingRadius=_radius;
	    var ax=xo + Math.cos(workingAngle / 180 * Math.PI) * workingRadius;
	    var ay=yo + Math.sin(-workingAngle / 180 * Math.PI) * workingRadius;
	    g.moveTo(ax, ay);
	    _svg+="M " + ax + "," + ay + " ";

	    // Loop for drawing exterior  curve segments
	    for (var i=0; i < segs; i++)
		{
			angle+=theta_a;
			var angleMid;
			angleMid=angle - (theta_a / 2);
			var bx=xo + Math.cos(angle) * workingRadius;
			var by=yo + Math.sin(angle) * workingRadius;
			var cx=xo + Math.cos(angleMid) * (workingRadius / Math.cos(theta_a / 2));
			var cy=yo + Math.sin(angleMid) * (workingRadius / Math.cos(theta_a / 2));
			g.quadraticCurveTo(cx, cy, bx, by);
			_svg+="Q " + cx + "," + cy + " " + bx + "," + by + " \n";
		}

		// draw a line from the end of the exterior curve to the start of the interior curve
		workingAngle+=workingArc;
		angle=-(workingAngle / 180) * Math.PI;

		// draw the interior (subtractive) wedge
		// draw a line from the center to the start of the interior curve
		var interiorRadius=workingRadius - _thickness;
		var dx=xo + Math.cos(workingAngle / 180 * Math.PI) * interiorRadius;
		var dy=yo + Math.sin(-workingAngle / 180 * Math.PI) * interiorRadius;
		g.lineTo(dx, dy);
		_svg+="L " + dx + "," + dy + " ";

		// Loop for drawing interior curve segments
		for (i=0; i < segs; i++)
		{
			angle+=theta_b;
			angleMid=angle - (theta_b / 2);
			bx=xo + Math.cos(angle) * interiorRadius;
			by=yo + Math.sin(angle) * interiorRadius;
			cx=xo + Math.cos(angleMid) * (interiorRadius / Math.cos(theta_b / 2));
			cy=yo + Math.sin(angleMid) * (interiorRadius / Math.cos(theta_b / 2));
			g.quadraticCurveTo(cx, cy, bx, by);
			_svg+="Q " + cx + "," + cy + " " + bx + "," + by + " \n";
		}
		g.lineTo(ax, ay);
		_svg+="L " + ax + "," + ay + " ";
        return _svg
	}
}

function doArcs(x, y){
    var canvas = document.getElementById("c");
    var context = canvas.getContext("2d");
    for(i=0; i<4; i++) {
        context.beginPath();
        drawArc(context, arcSettings(), x, y, 600, 600);
        context.fill();
        context.stroke();
    }
}

function arcSettings() {
    var a = {};
    a.startAngleA=Math.floor(Math.random() * 360);
    a.startAngleB=Math.floor(Math.random() * 360);
	a.arcAngleA=Math.floor(Math.random() * 360);
	a.arcAngleB=Math.floor(Math.random() * 360);
	a.radius=Math.floor(Math.random() * 270) + 70;
	a.thickness=Math.floor(Math.random() * (a.radius / 2));
    return a;
}

function main(){
    setInterval(function() {
        var canvas = document.getElementById("c");
        var context = canvas.getContext("2d");
        context.fillStyle = "rgba(255,255,255,0.3)";
        context.clearRect ( 0, 0, 1200 , 1200 );
        doArcs(600, 600);
    }, 1000);
}



