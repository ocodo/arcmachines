function Arc () {
    this.arcAngle = 0;
    this.arcAngleA = 0;
    this.arcAngleB = 0;
    this.startAngle = 0;
    this.startAngleA = 0;
    this.startAngleB = 0;
    this.radius = 0;
    this.thickness = 0;
    this.x = 0;
    this.y = 0;
    this.time = 0;
    this.duration = 0;
    this.c = 1;
    this.fillColors = ["rgba(255, 255, 255, 0.3)"];
    this.strokeColors = ["rgba(0, 0, 0, 0.5)"];
    this.fwd = true;

    this.minRadius = 20;
    this.maxRadius = 20;
    this.minThickness = 5;
    this.maxThickness = 45;
    this.minDuration = 160;
    this.maxDuration = 50;
}

Arc.prototype.draw = function(g) {
    var iRadius=this.radius - this.thickness;
    var radius = this.radius;
    var sAngle = this.startAngle * (Math.PI/180);
    var eAngle = ((this.startAngle + this.arcAngle) % 360) * (Math.PI/180);
    var ax = this.x + (radius * Math.cos(sAngle));
    var ay = this.y + (radius * Math.sin(sAngle));
    var cx = this.x + (iRadius * Math.cos(eAngle));
    var cy = this.y + (iRadius * Math.sin(eAngle));
    
    g.fillStyle = this.fillColors[0];
    g.strokeStyle = this.strokeColors[0];
    g.moveTo(ax, ay);
    g.arc(this.x, this.y, radius, sAngle, eAngle);
    g.lineTo(cx, cy);
    g.arc(this.x, this.y, iRadius, eAngle, sAngle, true);
    g.lineTo(ax, ay);
}

Arc.prototype.init = function(settings) {
    this.x = settings.x;
    this.y = settings.y;
}

Arc.prototype.shuffle = function() {
    this.arcAngle = Math.floor(Math.random() * 360);
    this.startAngle = Math.floor(Math.random() * 360);
    this.radius = Math.floor(Math.random() * this.maxRadius) + this.minRadius;
    this.thickness = this.clamp(Math.floor(Math.random() * (a.radius / 2)), this.minThickness, this.maxThickness);
}

Arc.prototype.randomSettings = function() {
    this.startAngleA = Math.floor(Math.random() * 360);
    this.startAngleB = Math.floor(Math.random() * 360);
    this.arcAngleA = Math.floor(Math.random() * 360);
    this.arcAngleB = Math.floor(Math.random() * 360);

    this.startAngle = this.startAngleA;
    this.arcAngle = this.arcAngleA;
    
    this.radius = Math.floor(Math.random() * this.maxRadius) + this.minRadius;
    this.thickness = this.clamp(Math.floor(Math.random() * (a.radius / 2)), this.minThickness, this.maxThickness);
}

Arc.prototype.ease = function(t, b, c, d) {
    return c*(t/=d) + b;
}   

Arc.prototype.animate = function(g) {
    this.startAngle = this.ease(this.time,this.startAngleA,this.startAngleB,this.duration);
    this.arcAngle = this.ease(this.time,this.arcAngleA,this.arcAngleB,this.duration);
    if(this.fwd)
    {this.time += this.c}
    else
    {this.time -= this.c}
    if(this.time >= this.duration)
    {this.fwd = false;}
    if(this.time < 0 && !this.fwd)
    {
        this.fwd = true;
        this.duration = Math.floor(Math.random()*this.maxDuration)+this.minDuration;
    }
    this.draw(g)
}

Arc.prototype.clamp = function(value, min, max) {
    return Math.min(Math.max(value, min), max);
}

function OcodoArcs(width, height, n, limit) {
    this.arcs = [];
    this.context;
    this.width = width;
    this.height = height;
    this.limit = limit;
    this.n = n;
}

OcodoArcs.prototype.doArcs = function(){
    this.context.clearRect(0,0,this.width,this.height);
    for(a in this.arcs) {
        this.context.beginPath();
        this.arcs[a].animate(context);
        this.context.fill();
        this.context.stroke();
    }
}

OcodoArcs.prototype.init = function(context){
    
    this.context = context;
    for(b=1; b<this.n+1; b++) {
        for(i=0; i<this.limit; i++) {
            var m={x:(b*77)-50, y:this.height/2};
            a = new Arc();
            a.init(m);
            console.log(a.x, a.y);
            a.randomSettings();
            a.time = 0;
            a.duration = Math.floor(Math.random()*a.maxDuration)+a.minDuration;
            this.arcs.push(a);
        }
    }
}

width = 600;
height = 80;

ocodo = new OcodoArcs(width, height, 5, 4);

function main(){
    canvas = document.getElementById("wave")
    context = canvas.getContext("2d");
    ocodo.init(context);
    setInterval(frame, 1000/30);
}

function frame(){
    ocodo.doArcs();
    t = context.getImageData(0,0,ocodo.width,ocodo.height/2);
    b = context.getImageData(0,ocodo.height/2,ocodo.width,ocodo.height);
    context.clearRect(0,0,ocodo.width, ocodo.height);
    context.putImageData(t, -30, 0);
    context.putImageData(b, 0, ocodo.height/2);
}
