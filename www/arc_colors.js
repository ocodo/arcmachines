function Arc () {
    this.arcAngle = 0;
    this.arcAngleA = 0;
    this.arcAngleB = 0;
    this.startAngle = 0;
    this.startAngleA = 0;
    this.startAngleB = 0;
    this.radius = 0;
    this.thickness = 0;
    this.x = 0;
    this.x = 0;
    this.width = 0;
    this.height = 0;
    this.time = 0;
    this.duration = 0;
    this.c = 1;
}

Arc.prototype.draw = function(g, fillColors) {

    var iRadius=this.radius - this.thickness;
    var radius = this.radius;

    var sAngle = this.startAngle * (Math.PI/180);
    var eAngle = ((this.startAngle + this.arcAngle) % 360) * (Math.PI/180);
    var ax = this.x + (radius * Math.cos(sAngle));// x  startAngle at radius
    var ay = this.y + (radius * Math.sin(sAngle));// y  startAngle at radius
    var cx = this.x + (iRadius * Math.cos(eAngle));// x  endAngle at iRadius
    var cy = this.y + (iRadius * Math.sin(eAngle));// y endAngle at iRadius

    f = iRadius/550;
    var findex = Math.floor(f * 3);

    g.fillStyle = fillColors[findex];
    g.strokeStyle = "#000";
    g.moveTo(ax, ay);
    g.arc(this.x, this.y, radius, sAngle, eAngle);
    g.lineTo(cx, cy);
    g.arc(this.x, this.y, iRadius, eAngle, sAngle, true);
    g.lineTo(ax, ay);
}

Arc.prototype.geometry = function(settings) {
    this.arcAngle = settings.arcAngle;
    this.arcAngleA = settings.arcAngle;
    this.arcAngleB = settings.arcAngleB;

    this.startAngle = settings.startAngle;
    this.startAngleA = settings.startAngle;
    this.startAngleB = settings.startAngleB;

    this.radius = settings.radius;
    this.thickness = settings.thickness;
}

Arc.prototype.init = function(settings) {
    this.y = settings.x;
    this.x = settings.x;
    this.width = settings.w;
    this.height = settings.h;
}

Arc.prototype.shuffle = function() {
    var a = arcSettings();
    this.arcAngle = a.arcAngle;
    this.startAngle = a.startAngle;
    this.radius = a.radius;
    this.thickness = a.thickness;
}

Arc.prototype.ease = function(t, b, c, d) {
    return c*(t/=d) + b;
}   

Arc.prototype.animate = function(g, fillColors) {
    this.startAngle = this.ease(this.time,this.startAngleA,this.startAngleB,this.duration);
    this.arcAngle = this.ease(this.time,this.arcAngleA,this.arcAngleB,this.duration);
    if(this.fwd)
    {this.time += this.c}
    else
    {this.time -= this.c}
    if(this.time >= this.duration)
    {this.fwd = false;}
    if(this.time < 0 && !this.fwd)
    {
        this.fwd = true;
        this.geometry(arcSettings());
        this.duration = Math.floor(Math.random()*50)+60;
    }
    this.draw(g, fillColors);
}

function doArcs(context, limit, fillColors){
    for(a in arcs) {
        context.beginPath();
        arcs[a].animate(context, fillColors);
        context.fill();
        context.stroke();
    }
}

function openAsBitmap() {
    canvas = document.getElementById("c");
    window.location = canvas.toDataURL("image/png");// -- save to image ->
}

function arcSettings() {
    var a = {};
    a.startAngle = Math.floor(Math.random() * 360);
    a.startAngleB = Math.floor(Math.random() * 360);
    a.arcAngle = Math.floor(Math.random() * 360);
    a.arcAngleB = Math.floor(Math.random() * 360);
    a.radius = Math.floor( Math.random() * 310) + 240;
    a.thickness = clamp(Math.floor(Math.random() * (a.radius / 2)), 10, 90) ;
    return a;
}

function clamp(value, min, max) {
    return Math.min(Math.max(value, min), max);
}

var limit=10;
var arcs=[], m={x:600, y:600, w:1200, h:1200};
for(i=0; i<limit; i++) {
    a = new Arc();
    a.init(m);
    a.geometry(arcSettings());
    a.time = 0;
    a.duration = Math.floor(Math.random()*50)+60;
    arcs.push(a);
}

function main(){

    var colors = [
        ["rgba(0, 0, 0, 0)", "rgba(255, 255, 255, 0.5)", "rgba(0, 170, 223, 0.75)"],
        ["rgba(0, 0, 0, 0)", "rgba(255, 255, 255, 0.5)", "rgba(0, 100, 193, 0.75)"],
        ["rgba(0, 0, 0, 0)", "rgba(255, 255, 255, 0.5)", "rgba(40, 120, 23, 0.75)"],
        ["rgba(0, 0, 0, 0)", "rgba(255, 255, 255, 0.5)", "rgba(150, 0, 0, 0.75)"],
        ["rgba(0, 0, 0, 0)", "rgba(255, 255, 255, 0.5)", "rgba(250, 100, 0, 0.75)"],
        ["rgba(0, 0, 0, 0)", "rgba(255, 255, 255, 0.5)", "rgba(80, 0, 70, 0.75)"],
        ["rgba(0, 0, 0, 0)", "rgba(255, 255, 255, 0.5)", "rgba(78, 46, 40, 0.75)"] 
    ];
    var i;
    var canvas = document.getElementById("c");
    var context = canvas.getContext("2d");
    var fillColors = colors[Math.floor(Math.random()*colors.length)];
    console.log(fillColors);
    
    setInterval(function() {
        context.fillStyle = "rgba(255,255,255,0.7)";
        context.clearRect ( 0, 0, 1200 , 1200 );
        doArcs(context, limit, fillColors);
    }, 1000/30);              
}
