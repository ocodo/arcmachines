function Arc () {
    this.arcAngle = 0;
    this.arcAngleA = 0;
    this.arcAngleB = 0;
    this.startAngle = 0;
    this.startAngleA = 0;
    this.startAngleB = 0;
    this.radius = 0;
    this.thickness = 0;
    this.x = 0;
    this.y = 0;
    this.time = 0;
    this.duration = 0;
    this.c = 1;
    this.fillColors = ["rgba(255, 255, 255, 0.3)"];
    this.strokeColors = ["rgba(0, 0, 0, 0.9)"];
    this.fwd = true;

    this.minRadius = 50;
    this.maxRadius = 70;
    this.minThickness = 10;
    this.maxThickness = 90;
    this.minDuration = 60;
    this.maxDuration = 50;
}

Arc.prototype.draw = function(g) {
    var iRadius=this.radius - this.thickness;
    var radius = this.radius;
    var sAngle = this.startAngle * (Math.PI/180);
    var eAngle = ((this.startAngle + this.arcAngle) % 360) * (Math.PI/180);
    var ax = this.x + (radius * Math.cos(sAngle));
    var ay = this.y + (radius * Math.sin(sAngle));
    var cx = this.x + (iRadius * Math.cos(eAngle));
    var cy = this.y + (iRadius * Math.sin(eAngle));
    
    g.fillStyle = this.fillColors[0];
    g.strokeStyle = this.strokeColors[0];
    g.moveTo(ax, ay);
    g.arc(this.x, this.y, radius, sAngle, eAngle);
    g.lineTo(cx, cy);
    g.arc(this.x, this.y, iRadius, eAngle, sAngle, true);
    g.lineTo(ax, ay);
}

Arc.prototype.geometry = function(settings) {
    this.arcAngle = settings.arcAngle;
    this.arcAngleA = settings.arcAngle;
    this.arcAngleB = settings.arcAngleB;
    
    this.startAngle = settings.startAngle;
    this.startAngleA = settings.startAngle;
    this.startAngleB = settings.startAngleB;
    
    this.radius = settings.radius;
    this.thickness = settings.thickness;
}

Arc.prototype.init = function(settings) {
    this.x = settings.x;
    this.y = settings.y;
}


Arc.prototype.shuffle = function() {
    this.arcAngle = Math.floor(Math.random() * 360);
    this.startAngle = Math.floor(Math.random() * 360);
    this.radius = Math.floor(Math.random() * this.maxRadius) + this.minRadius;
    this.thickness = this.clamp(Math.floor(Math.random() * (a.radius / 2)), this.minThickness, this.maxThickness);
}

Arc.prototype.ease = function(t, b, c, d) {
    return c*(t/=d) + b;
}   

Arc.prototype.animate = function(g) {
    this.startAngle = this.ease(this.time,this.startAngleA,this.startAngleB,this.duration);
    this.arcAngle = this.ease(this.time,this.arcAngleA,this.arcAngleB,this.duration);
    if(this.fwd)
    {this.time += this.c}
    else
    {this.time -= this.c}
    if(this.time >= this.duration)
    {this.fwd = false;}
    if(this.time < 0 && !this.fwd)
    {
        this.fwd = true;
        this.geometry(this.randomSettings());
        this.duration = Math.floor(Math.random()*this.maxDuration)+this.minDuration;
    }
    this.draw(g)
}

Arc.prototype.clamp = function(value, min, max) {
    return Math.min(Math.max(value, min), max);
}

Arc.prototype.randomSettings = function() {
    var a = {};
    a.startAngle = Math.floor(Math.random() * 360);
    a.startAngleB = Math.floor(Math.random() * 360);
    a.arcAngle = Math.floor(Math.random() * 360);
    a.arcAngleB = Math.floor(Math.random() * 360);
    a.radius = Math.floor(Math.random() * this.maxRadius) + this.minRadius;
    a.thickness = this.clamp(Math.floor(Math.random() * (a.radius / 2)), this.minThickness, this.maxThickness) ;
    return a;
}

function OcodoArcs() {
    this.arcs = [];
    this.context;
}

OcodoArcs.prototype.doArcs = function(limit){
    this.context.clearRect(0,0,1200,1200);
    for(a in this.arcs) {
        this.context.beginPath();
        this.arcs[a].animate(context);
        this.context.fill();
        this.context.stroke();
    }
}

OcodoArcs.prototype.init = function(context){
    this.context = context;
    for(b=1; b<6; b++) {
        for(i=0; i<6; i++) {
            var m={x:b*232-105, y:600};
            a = new Arc();
            a.init(m);
            console.log(a.x, a.y);
            a.geometry(a.randomSettings());
            a.time = 0;
            a.duration = Math.floor(Math.random()*50)+60;
            this.arcs.push(a);
        }
    }
}

ocodo = new OcodoArcs();

function main(){
    canvas = document.getElementById("c")
    context = canvas.getContext("2d");
    ocodo.init(context);
    setInterval(frame, 1000/30);
}

function frame(){
    ocodo.doArcs(6);
}
