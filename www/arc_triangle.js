function Arc () {
    this.arcAngle = 0;
    this.arcAngleA = 0;
    this.arcAngleB = 0;
    this.startAngle = 0;
    this.startAngleA = 0;
    this.startAngleB = 0;
    this.radius = 0;
    this.thickness = 0;
    this.x = 0;
    this.y = 0;
    this.width = 0;
    this.height = 0;
    this.time = 0;
    this.duration = 0;
    this.c = 1;
    this.fillColors = ["rgba(255, 255, 255, 0.3)"];
    this.strokeColors = ["#000000"];
    this.fwd = true;
    
}

Arc.prototype.draw = function(g) {
    var iRadius=this.radius - this.thickness;
    var radius = this.radius;
    var sAngle = this.startAngle * (Math.PI/180);
    var eAngle = ((this.startAngle + this.arcAngle) % 360) * (Math.PI/180);
    var ax = this.x + (radius * Math.cos(sAngle));// x  startAngle at radius
    var ay = this.y + (radius * Math.sin(sAngle));// y  startAngle at radius
    var cx = this.x + (iRadius * Math.cos(eAngle));// x  endAngle at iRadius
    var cy = this.y + (iRadius * Math.sin(eAngle));// y endAngle at iRadius
    
    g.fillStyle = this.fillColors[0];
    g.strokeStyle = this.strokeColors[0];
    g.moveTo(ax, ay);
    g.arc(this.x, this.y, radius, sAngle, eAngle);
    g.lineTo(cx, cy);
    g.arc(this.x, this.y, iRadius, eAngle, sAngle, true);
    g.lineTo(ax, ay);
}

Arc.prototype.geometry = function(settings) {
    this.arcAngle = settings.arcAngle;
    this.arcAngleA = settings.arcAngle;
    this.arcAngleB = settings.arcAngleB;
    
    this.startAngle = settings.startAngle;
    this.startAngleA = settings.startAngle;
    this.startAngleB = settings.startAngleB;
    
    this.radius = settings.radius;
    this.thickness = settings.thickness;
}

Arc.prototype.init = function(settings) {
    this.x = settings.x;
    this.y = settings.y;
    this.width = settings.w;
    this.height = settings.h;
}

Arc.prototype.shuffle = function() {
    var a = arcSettings();
    this.arcAngle = a.arcAngle;
    this.startAngle = a.startAngle;
    this.radius = a.radius;
    this.thickness = a.thickness;
}

Arc.prototype.ease = function(t, b, c, d) {
    return c*(t/=d) + b;
}

Arc.prototype.animate = function(g) {
    this.startAngle = this.ease(this.time,this.startAngleA,this.startAngleB,this.duration);
    this.arcAngle = this.ease(this.time,this.arcAngleA,this.arcAngleB,this.duration);
    if(this.fwd)
    {this.time += this.c}
    else
    {this.time -= this.c}
    if(this.time >= this.duration)
    {this.fwd = false;}
    if(this.time < 0 && !this.fwd)
    {
        this.fwd = true;
        this.geometry(arcSettings());
        this.duration = Math.floor(Math.random()*50)+60;
    }
    this.draw(g)
}

function doArcs(context, limit){
    for(a in arcs) {
        context.beginPath();
        arcs[a].animate(context);
        context.fill();
        context.stroke();
    }
}

function openAsBitmap() {
    canvas = document.getElementById("c");
    window.location = canvas.toDataURL("image/png");// -- save to image ->
}

function arcSettings() {
    var a = {};
    a.startAngle = Math.floor(Math.random() * 360);
    a.startAngleB = Math.floor(Math.random() * 360);
    a.arcAngle = Math.floor(Math.random() * 360);
    a.arcAngleB = Math.floor(Math.random() * 360);
    a.radius = Math.floor(Math.random() * 120) + 70; // == max diameter of 1100;
    a.thickness = clamp(Math.floor(Math.random() * (a.radius / 2)), 10, 90) ;
    return a;
}

function clamp(value, min, max) {
    return Math.min(Math.max(value, min), max);
}

var limit=6;
var arcs=[];
var positions=[{x:600,y:363},{x:307,y:865},{x:889,y:865}]
for(b=0; b<3; b++) {
    
    for(i=0; i<limit; i++) {
        a = new Arc();
        a.init(positions[b]);
        console.log(a.x, a.y);
        a.geometry(arcSettings());
        a.time = 0;
        a.duration = Math.floor(Math.random()*50)+60;
        arcs.push(a);
    }
}
function main(){
    
    var i;
    var canvas = document.getElementById("c");
    var context = canvas.getContext("2d");
    
    setInterval(function() {
                context.clearRect ( 0, 0, 1200 , 1200 );
                doArcs(context, limit);
                }, 1000/30);
    
}
